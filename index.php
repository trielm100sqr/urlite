<?php
	
require('urlite.class.php');

// check if short URL is requested or not
// if yes parse the request and return what needs to be returned
if (isset($_GET['code'])) {

	$code = $_GET['code'];
	$urlite = new URLite();
	$check = $urlite->parseURLCode($code);
	
	// if this request aims to go to the destination page, then
	// update total visiting times, and also redirect users to destination page
	if ($check == 0) {
		$urlite->updateVisit($code);
		$urlite->redirectURL($code);
	}
	
	// if request is unknown, then return bad request
	else if ($check == -1) { echo "Bad request. This page isn't featured on URLite!"; }
	
	// this is to handle the request of viewing the total number of hits
	// that the users have made to the short URL
	else {
		$urlcode = $check[0];
		$info = $urlite->getInfofromCode($urlcode);
		include('stats.php');
	}
}

// if not, display the main URLite page for URL shortening
else {
	include('create.php');
}

?>
