function createXMLHttp() 
{
	if (typeof XMLHttpRequest != "undefined") {
		return new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		var aVersions = [ "MSXML2.XMLHttp.5.0",
		"MSXML2.XMLHttp.4.0","MSXML2.XMLHttp.3.0",
		"MSXML2.XMLHttp","Microsoft.XMLHttp"
		];
		for (var i = 0; i < aVersions.length; i++) {
			try {
			var oXmlHttp = new ActiveXObject(aVersions[i]);
			return oXmlHttp;
			} catch (oError) {
			}
		}
	}
	throw new Error("XMLHttp object couldn't be created.");
}

function is_valid_url(url) {
     var tomatch= /http:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/
     if (tomatch.test(url)) return true;
     else return false; 
}

function check_form_input(url, token) {
	if ((url == '') || (token == '')) {
		return false;
	}
	return true;
}

function createURlite(url, token) {
	if (!check_form_input(url.value, token.value)) {
		document.getElementById("display").innerHTML = '<span class="inp_error">Please fill in both text-boxes above</span>';
		return false;
	}
	if (!is_valid_url(url.value)) {
		document.getElementById("display").innerHTML = '<span class="inp_error">Your URL is invalid. Please enter again.</span>';
		return false;
	}
	document.getElementById("display").innerHTML = "<img src='images/ajax-loader.gif' alt='Loading' />";
	var oXmlHttp = createXMLHttp();
	oXmlHttp.open("get", "process.php?url="+encodeURIComponent(url.value)+"&token="+token.value, true);
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				document.getElementById("display").innerHTML = "&nbsp;";
				document.getElementById("surl").value = oXmlHttp.responseText;
			} else {
				document.getElementById("display").innerHTML = "An error occurred: "+ oXmlHttp.statusText;
			}
		}
	};
	oXmlHttp.send(null);  
}