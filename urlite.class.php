<?php

/**
 * 	DESCRIPTION
 * 		URLITE is a web-based application that helps users to create shorter URLs
 * 		to share on Twitter or Facebook. The use of this tool is similar to 
 * 		TinyURL and Bitly; however, it allows users to shorten URLs that based on
 * 		their domain names. So, instead of getting something like bit.ly/AbC,
 * 		the users are now able to get something like mydomain.com/AbC.
 * 
 * 	AUTHOR
 * 		Developed by Tris Le
 * 		Email: trisle.vn@gmail.com
 * 
 * 	VERSION 1.1
 * 	VERSION 1.2
 * 
 * 	
 */

class URLite 
{
	
	// base URL is a full absolute URL to your installed URLite folder, followed by a slash
	public $baseURL = 'http://localhost/urlite/';
	
	// password is set to compare with the token, you can set it to whatever you like
	private $password = 'urlite';
	
	private $dbname = 'urlite_db';
	
	private $sqlitedb;
	
	// destination URL
	private $nurl = '';

	// user entered token	
	private $token = '';
	
	// a distintive 3-or-more-character code that comes along with each short URL
	public $surlCode;
	
	//check if you enter the right token
	public $activate = true; 
	
	/**
	 * If token is entered correctly, then initialize some basic instance variables,
	 * otherwise return a message "wrong token".
	 * Also connect to the SQLite database
	 * @param $url is user entered raw URL
	 * @param $token is user entered token
	 */
	public function __construct($url = '', $token = '') {
		if (($url != '') && ($token != '')) {
			if ($token != $this->password) {
				$this->activate = false;
			}
			else {
				$this->nurl = $url;
				$this->token = $token;
			}
		}
		$this->SQLiteConnect();
	}
	
	public function shortenThisURL() {
		$this->surlCode = $this->setURLCode();
		$this->shortenURLviaDB();
	}
	
	/**
	 * Attempt to create a 3-or-more-character code that comes along with the short URL.
	 * If the code is already existed, another code will be generated.
	 * return an encrypted code for entered URL.
	 */
	private function setURLCode() {
		$cnt = 3;
		$enc = substr(md5($this->nurl.$this->token), 0, $cnt);
		$query = sqlite_query($this->sqlitedb, "SELECT * FROM url WHERE code = '".$enc."'");
		$result = sqlite_fetch_array($query);
		if ($result['raw_url'] == $this->nurl) {
			return $enc;
		}
		else {
			while (sqlite_fetch_array($query)) {
				$cnt++;
				$enc = substr(md5($this->nurl.$this->token), 0, $cnt);
				$query = sqlite_query($this->sqlitedb, "SELECT * FROM url WHERE code = '".$enc."'");
			}
		}
		return $enc;
	}
	
	/**
	 * Connect to SQLite database.
	 * @param $db is user-specified name of the database
	 */
	private function SQLiteConnect($db = '')  {
		if ($db != '') { $this->dbname = $db; }
		if (!file_exists($this->dbname)) {
			$this->sqlitedb = sqlite_open($this->dbname, 0666, $sqliteerror);
			if ($this->sqlitedb) {
				$create_url_db = 'CREATE TABLE url (raw_url TEXT,
													code VARCHAR(32),
													token VARCHAR(12),
													total_hits INTEGER)';
				sqlite_query($this->sqlitedb, $create_url_db);
			} 
			else {
	    		die($sqliteerror);
			}
		}
		$this->sqlitedb = sqlite_open($this->dbname, 0666, $sqliteerror);
	}

	/*
	 * Insert entered URL and its generated shorter URL into the SQLite database
	 */
	private function shortenURLviaDB() {
		$insertURL = "INSERT INTO url VALUES('".$this->nurl."','".$this->surlCode."','".$this->token."',0)";
		sqlite_query($this->sqlitedb, $insertURL);
	}
	
	/**
	 * Check the request to short URL.
	 * return 0 if no request to short URL
	 * return -1 if bad request and
	 * return an array of information if request is correct
	 */
	public function parseURLCode($code) {
		$check = explode("-", $code);
		if (count($check) == 1) {
			return 0;
		}
		else {
			if ($check[1] == "hit") {
				return $check;
			}
			else {
				return -1;
			}
		}
	}
	
	/**
	 * Update the total hits everytime a person visit the link
	 * @param $code is the code provided to each short URL
	 */
	public function updateVisit($code) {
		$update = "UPDATE url SET total_hits = total_hits + 1 WHERE code = '".$code."'";
		sqlite_query($this->sqlitedb, $update);
	}
	
	/**
	 * Redirect to the destination page
	 * @param $code is the code provided to each short URL
	 */
	public function redirectURL($code) {
		$select = "SELECT * FROM url WHERE code = '".$code."'";
		$result = sqlite_query($this->sqlitedb, $select);
		$redirect = sqlite_fetch_array($result, SQLITE_ASSOC);
		header ('HTTP/1.1 301 Moved Permanently');
		header("Location: ".$redirect['raw_url']);
	}
	
	/**
	 * Return an info. array (i.e. destination url, total hits) of a short URL
	 * @param $code is the code provided to each short URL
	 */
	public function getInfofromCode($code) {
		$select = "SELECT * FROM url WHERE code = '".$code."'";
		$result = sqlite_query($this->sqlitedb, $select);
		$ret = sqlite_fetch_array($result, SQLITE_ASSOC);
		$ret_array = array();
		$ret_array[0] = $ret['raw_url'];
		$ret_array[1] = $ret['total_hits'];
		return $ret_array;
	}
	
	public function __destruct() {
	}

}

?>