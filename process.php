<?php
	
require('urlite.class.php');

$url = $_GET['url'];

$token = $_GET['token'];

$surl = new URLite($url, $token);

// Check if token is entered correctly, in comparison to the password
if ($surl->activate) {
	$surl->shortenThisURL();
	echo $surl->baseURL.$surl->surlCode."/";
}
else {
	echo "Wrong token. Please type in again.";
}

?>
