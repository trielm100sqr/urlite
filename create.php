<!doctype html>
<html> 
<head>
	<title>URLite - Simplify your URL</title>
	<link rel="stylesheet" type="text/css" href="styles/create.css">
	<script type="text/javascript" src="js/jsfunctions.js"></script>
</head>
<body>
	<div id="container">
		<img src="images/logo.png" />	
		<form action="#createURLite" method="GET" id="URLiteForm" name="URLiteForm">
			
			<div id="demo_notice">
				Demo version of <b>URLite v1.2</b>
				<br />
				Enter token "<b>urlite</b>" (without quotes) to test this application
				<br />
				Tip: simply add "-hit" (without quotes) at the end of the directory name to view total visits of your shortened URL e.g. <span style="color: navy">http://trisle.net/u/fed<b>-hit/</b></span>
			</div>
			
			<br />
			
			<label>Enter your super-extra-extremely-long URL:</label>
			<input type="text" name="url" id="url" class="text" />

			<label>Enter the secret token (password):</label>
			<input type="text" id="token" name="token" class="text" />

			<label>This is your shortened URL: (Read-only)</label>
			<input type="text" name="surl" id="surl" onClick="select()" value="This is where your URL will be shortened" readonly="readonly" class="text shortened" />
			
			<div id="display"></div>

			<div class="submit">
				<input type="button" value="Transform" onclick="createURlite(document.URLiteForm.url, document.URLiteForm.token)" />
			</div>

		</form>
		<div class="footer">
			Copyright &copy 2011 <strong>URLite 1.2</strong>. <br />
			This website supports Firefox, Chrome and Internet Explorer.
		</div>
	</div>
</body>
</html>