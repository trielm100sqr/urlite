<!doctype html>
<html> 
<head>
	<title>Total hits of this link</title>
	<link rel="stylesheet" type="text/css" href="../styles/stats.css">
</head>
<body>
	<div id="hits_tbl">
		<li>Your original url is <strong><?php echo $info[0]; ?></strong></li>
		<li>Your URLited url is <strong><?php echo $urlite->baseURL . $urlcode; ?>/</strong></li>
		<li>It has been visited <strong><?php echo $info[1]; ?></strong> times</li>
		<li><a href="<?php echo $urlite -> baseURL; ?>">Click here to return to main page</a></li>
	</div>
</body>
</html>